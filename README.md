# json_reader_stepanov

Reads test json file and prints records

## Run

sbt package

spark-submit --master local[*] --class com.github.mrpowers.my.cool.project.JsonReader target/scala-XX/json_reader_stepanov_XX.jar <path_to_winemag-data-130k-v2.json>
