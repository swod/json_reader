package com.github.mrpowers.my.cool.project
import org.json4s._
import org.json4s.jackson.JsonMethods._


object JsonReader {
  def main (args: Array[String]) {
    println(">Starting application<")
    val filename = args(0)
    println("File: " + filename)

    val parser = new JsonParser()
    parser.showData(filename)
    println(">Finished<")
  }
}


class JsonParser extends SparkSessionWrapper {

  import spark.implicits._

  def showData(filename: String) {
    val lines = spark.sparkContext.textFile(filename)
    println("Line count: " + lines.count())
    lines.foreach(line => {
      implicit val formats = DefaultFormats

      val item = parse(line).extract[WineRecord]
      println(item)
    })
  }

}

case class WineRecord(
  id: Option[Int], country: Option[String], points: Option[Int],
  title: Option[String], variety: Option[String],
  winery: Option[String], price: Option[Int]
)
